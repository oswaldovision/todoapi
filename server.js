const express = require('express')
const bodyParser = require('body-parser')

const port = process.env.PORT || 3000
const app = express()
app.use(bodyParser.json())
app.set('view engine', 'pug')

const todosRouter = require("./routes/todo")

app.get('/', function (req, res) {
  res.render('index', { title: 'Hey', message: 'Hello there!' })
})


app.use("/todo", todosRouter)

app.listen(port, () => {
  console.log(`Server listening: ${port}`)
})

module.exports = {app}